//Send ajax request with XMLHttpRequest.
const form = document.querySelector('form');
form.onsubmit = function(e) {
	e.preventDefault();
	const xhr = new XMLHttpRequest();
	const n = document.querySelector('#n').value;
	document.querySelector('.result').innerHTML =
		'<img class="loading" src="https://thumbs.gfycat.com/NextDefenselessHornbill-small.gif" />';
	xhr.open('POST', 'calculate.php', true);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhr.send(`n=${n}`);

	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && xhr.status == 200) {
			const response = document.querySelector('.result');
			console.log(this);
			response.innerHTML = this.responseText;
			console.log(this.responseText);
		}
	};
};
//With this function we clear section of result.
const clear = document.querySelector('.clear');
clear.onclick = function() {
	let n = document.querySelector('#n');
	let result = document.querySelector('.result');
	n.value = '';
	result.innerHTML = '';
};
