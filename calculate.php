<?php 
    sleep(2);
    //Get factorial n value.
    if ( isset($_POST["n"]) ) {
        if ( $_POST["n"] == '' ) {
          echo "<div class='error'>Please fill out the missing fields</div>";
        } else {
            $n = $_POST["n"];
            if ( $n == 0 ) {
                echo "<div class='response'>1</div>";
            } 
            else if ( $n < 0 ) {
                echo "<div class='error'>n must be > or = to 0</div>";
            } else {
                $factorial = 1;
                for ( $i = 1; $i <= $n; $i++ ) {
                    $factorial = $factorial * $i;
                }
                echo "<div class='response'>$factorial</div>";
            }
        }
    }
?>